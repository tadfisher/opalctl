This directory is added to include search path by ../meson.build.

It's useful when sed-opal.h in your system does not yet include the new OPAL
IOCTLs. Just copy include/uapi/linux/sed-opal.h from sufficiently new Linux
kernel sources and place the file in `extra-includes/linux/sed-opal.h`.
