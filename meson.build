project('opalctl', 'c', default_options: ['c_std=c11'])

add_project_arguments(
	[
		'-D_POSIX_C_SOURCE=200809L',
		'-D_XOPEN_SOURCE=500'
	],
	language: 'c'
)

libcifra = static_library(
	'cifra',
	[
		'cifra/src/pbkdf2.h',
		'cifra/src/pbkdf2.c',
		'cifra/src/sha1.h',
		'cifra/src/sha1.c',
		'cifra/src/blockwise.h',
		'cifra/src/blockwise.c',
		'cifra/src/bitops.h',
		'cifra/src/chash.h',
		'cifra/src/chash.c',
		'cifra/src/hmac.h',
		'cifra/src/hmac.c',
		'cifra/src/ext/handy.h',
		'cifra/src/tassert.h'
	],
	include_directories: include_directories('cifra/src', 'cifra/src/ext'),
	override_options: ['c_std=gnu11']
)

cifra = declare_dependency(
	link_with: libcifra,
	include_directories: include_directories('cifra/src')
)

libopalioctl = static_library(
	'opalioctl',
	[
		'src/libopalioctl/DiskSerialNo.h',
		'src/libopalioctl/DiskSerialNo.c',
		'src/libopalioctl/PasswordHash.h',
		'src/libopalioctl/PasswordHash.c',
		'src/libopalioctl/ReadPassword.h',
		'src/libopalioctl/ReadPassword.c',
		'src/libopalioctl/OpalResult.h',
		'src/libopalioctl/OpalResult.c',
		'src/libopalioctl/OpalUtils.h',
		'src/libopalioctl/OpalUtils.c',
		'src/libopalioctl/Opal.h',
		'src/libopalioctl/Opal.c'
	],
	include_directories: include_directories('extra-includes'),
	dependencies: cifra
)

opalioctl = declare_dependency(
	link_with: libopalioctl
)

executable(
	'opalctl',
	[
		'src/opalctl.c'
	],
	dependencies: opalioctl,
	install: true
)

executable(
	'opalpba',
	[
		'src/opalpba.c'
	],
	dependencies: [ opalioctl ],
	install: false
)
