# opalctl
`opalctl` is a command line tool that can issue Opal commands to SED Opal drives using Linux kernel IOCTLs.

Probably the most popular way to control Opal drives in Linux is by using [sedutil](https://github.com/Drive-Trust-Alliance/sedutil). sedutil works by sending Opal ATA commands directly to a drive. This requires running Linux with the kernel argument `libata.allow_tpm=1`.

There is [sed-opal-temp](https://github.com/ScottyBauer/sed-opal-temp) (and a [fork](https://github.com/ghostav/sed-opal-temp) which adds some more IOCTLs) which uses Linux kernel Opal IOCTLs.

Both of these are quite powerful. sedutil can do some things for which there are no IOCTLs available. sed-opal-temp can do everything that the IOCTLs allow but it seems to be stuck in a "temp" limbo. Furthermore, if you set up the password with sedutil then you likely can't use sed-opal-temp with the same drive: sedutil by default uses password hash as the Opal key while sed-opal-temp uses a password directly (although it is possible to ask sedutil to not hash the password). I wanted a simple tool for basic usecases that is compatible with sedutil but which uses SED Opal IOCTLs - that's why I created opalctl.

This repository contains both the tool `opalctl` and also a "pre-boot authorization" environment (similar to [LinuxPBA](https://github.com/Drive-Trust-Alliance/sedutil/tree/master/LinuxPBA) from sedutil) called `opalpba`. The tools are written in C to minimize dependencies and make it easier to include them in an initramfs.

## Kernel Requirements
`opalctl` needs Kernel with SED Opal enabled (`CONFIG_BLK_SED_OPAL`). It also needs some extra Opal IOCTLs that were not yet added to the Linux kernel. With some luck this changes in the future. For now, you can build Linux kernel with [these patches](https://lore.kernel.org/lkml/1549054223-12220-1-git-send-email-zub@linux.fjfi.cvut.cz/). You can get the patches also from [this git repo](https://github.com/zub2/linux/tree/opal-zub-v6-submit).

## Cloning
Please note that this repository uses [git submodule](https://git-scm.com/docs/git-submodule) to fetch [cifra](https://github.com/ctz/cifra/). So either clone this repository with `--recurse-submodules`, or do `git submodule init && git submodule update` after checkout.

## Compiling
You need [meson](http://mesonbuild.com/), [ninja](https://ninja-build.org/) and a C compiler.

You also need a new-enough kernel headers. If your build fails with complaints about missing `IOC_OPAL_MBR_STATUS` and `IOC_OPAL_WRITE_SHADOW_MBR` like:
```
../src/libopal/Opal.c: In function ‘opalSetShadowMBRDone’:
../src/libopal/Opal.c:158:23: error: ‘IOC_OPAL_MBR_STATUS’ undeclared (first use in this function); did you mean ‘IOC_OPAL_LR_SETUP’?
```
then your `sed-opal.h` in Linux headers does not contain all the required IOCTLs. As a work-around, copy `include/uapi/linux/sed-opal.h` from latest Linux kernel and place the file in `extra-includes/linux/sed-opal.h`.

To build a static executable, just pass `CFLAGS=-static` to meson, e.g.:

```shell
CFLAGS=-static meson build
```
and then build with ninja, e.g.:
```shell
cd build && ninja
```
With some luck you get the two executables, `opalctl` and `opalpba` in the build directory.

## opalctl tool
Run `opalctl help` to list available subcommands. Run `opalctl SUBCOMMAND -h` to get more info about a subcommand.

All operations that require a password normally use a sedutil-style password hash as the Opal key. If you want to use the password as the key, use the option `-d`. This treats passwords in the same way as sed-opal-temp does.

## opalpba tool
`opalpba` is intended to be used in a pre-boot authorization environment - typically in a dedicated initramfs that is responsible for asking user for a password and then unlocking the boot disk.

When run, `opalpba` asks for a password and uses that to:

1. unlock locking range 0
1. set shadow-mbr-done flag to true (thus hiding the shadow MBR)

It's possible to customize this:

* `-l LOCKING_RANGE` specifies the locking range number (when not specified, locking range 0 is unlocked)
* `-d` disables password hashing, making the password handling compatible with sed-opal-temp
* `-v` makes opalpba more verbose (by default it suppresses some errors, like an auto-detected block device that does not support OPAL, or a wrong password, to not give away more info than necessary)
* you can explicitly specify the block device(s) to unlock on the command line, e.g. `opalpba /dev/sda`; when not specified, opalpba tries to unlock all block devices
