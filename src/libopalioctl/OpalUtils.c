/*
 * Copyright 2019 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of opalctl.
 *
 * opalctl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * opalctl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with opalctl.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "OpalUtils.h"

#include <stdlib.h>

void freePassword(char *password)
{
	if (password != NULL)
	{
		for (char *p = password; *p != 0; p++)
			*p = 0;

		free(password);
	}
}

void freeKey(uint8_t *key, size_t size)
{
	if (key != NULL)
	{
		for (size_t i = 0; i < size; i++)
			key[i] = 0;

		free(key);
	}
}
