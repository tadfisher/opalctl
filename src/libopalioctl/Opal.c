/*
 * Copyright 2019 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of opalctl.
 *
 * opalctl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * opalctl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with opalctl.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Opal.h"

#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <sys/mman.h>

#include <linux/sed-opal.h>

// from linux/include/linux/errno.h, AFAIK not available from userspace
#define LINUX_ENOTSUP 524

#define OPAL_STATUS_OK 0

static bool setOPALKey(struct opal_key * opalKey, uint8_t lockingRegion, const uint8_t * key, size_t keySize)
{
	if (keySize > OPAL_KEY_MAX)
	{
		return false;
	}

	opalKey->lr = lockingRegion;

	memcpy(opalKey->key, key, keySize);
	opalKey->key_len = keySize;

	return true;
}

static OpalResult opalIoctl(int fd, unsigned long request, void * data)
{
	const int r = ioctl(fd, request, data);

	OpalResult result =
	{
		OpalSuccess,
		0
	};

	switch (r)
	{
	case -1:
		if (errno == LINUX_ENOTSUP) // this is different from userspace's ENOTSUP :-(
			result.status = OpalNotAnOpalDevice;
		else if (errno == ENOTTY)
			result.status = OpalNotSupported; // not supported by kernel (unknown IOCTL)
		else
		{
			// let's hope it's something that works with userspace errno...
			result.status = OpalErrnoError;
			result.errNo = errno;
		}
		break;

	case OPAL_STATUS_OK:
		result.status = OpalSuccess;
		break;

	default:
		result.status = OpalUnknownError;
		break;
	}

	return result;
}

OpalResult opalLockUnlock(int fd, const char * device, uint8_t lockingRegion, const uint8_t * key, size_t keySize, OpalLockState lockState)
{
	struct opal_lock_unlock opalLockUnlock =
	{
		.session =
		{
			.sum = 0, // not single user mode
			.who = OPAL_ADMIN1
		}
	};

	switch (lockState)
	{
	case OpalRW:
		opalLockUnlock.l_state = OPAL_RW;
		break;
	case OpalRO:
		opalLockUnlock.l_state = OPAL_RO;
		break;
	case OpalLocked:
		opalLockUnlock.l_state = OPAL_LK;
		break;
	default:
		{
			OpalResult r = { .status = OpalInvalidArgument };
			return r;
		}
	}

	if (!setOPALKey(&opalLockUnlock.session.opal_key, lockingRegion, key, keySize))
	{
		OpalResult r = { .status = OpalUnknownError };
		return r;
	}

	return opalIoctl(fd, IOC_OPAL_LOCK_UNLOCK, &opalLockUnlock);
}

OpalResult opalEnableDisableShadowMBR(int fd, const char * device, const uint8_t * key, size_t keySize, bool enabled)
{
	struct opal_mbr_data opalMBRData =
	{
		.enable_disable = enabled ? OPAL_MBR_ENABLE : OPAL_MBR_DISABLE
	};

	if (!setOPALKey(&opalMBRData.key, 0, key, keySize))
	{
		OpalResult r = { .status = OpalUnknownError };
		return r;
	}

	return opalIoctl(fd, IOC_OPAL_ENABLE_DISABLE_MBR, &opalMBRData);
}

OpalResult opalSetShadowMBRDone(int fd, const char * device, const uint8_t * key, size_t keySize, bool done)
{
	struct opal_mbr_done opalMBRDone =
	{
		.done_flag = done ? OPAL_MBR_DONE : OPAL_MBR_NOT_DONE
	};

	if (!setOPALKey(&opalMBRDone.key, 0, key, keySize))
	{
		OpalResult r = { .status = OpalUnknownError };
		return r;
	}

	return opalIoctl(fd, IOC_OPAL_MBR_DONE, &opalMBRDone);
}

OpalResult opalWriteMBR(int fd, const char * device, const uint8_t * key, size_t keySize, const char * mbrFile)
{
	int mbrFd = open(mbrFile, O_RDONLY | O_CLOEXEC);
	if (mbrFd < 0)
	{
		OpalResult r = { .status = OpalUnknownError };
		return r;
	}

	struct stat mbrSt;
	if (fstat(mbrFd, &mbrSt) == -1)
	{
		close(mbrFd);
		OpalResult r = { .status = OpalUnknownError };
		return r;
	}

	void * mbrMmap = mmap(NULL, mbrSt.st_size, PROT_READ, MAP_PRIVATE, mbrFd, 0);
	if (mbrMmap == MAP_FAILED)
	{
		close(mbrFd);
		OpalResult r = { .status = OpalUnknownError };
		return r;
	}

	struct opal_shadow_mbr opalShadowMBR =
	{
		.data = (uintptr_t)mbrMmap,
		.offset = 0,
		.size = mbrSt.st_size
	};

	if (!setOPALKey(&opalShadowMBR.key, 0, key, keySize))
	{
		munmap(mbrMmap, mbrSt.st_size);
		close(mbrFd);
		OpalResult r = { .status = OpalUnknownError };
		return r;
	}

	const OpalResult result = opalIoctl(fd, IOC_OPAL_WRITE_SHADOW_MBR, &opalShadowMBR);

	munmap(mbrMmap, mbrSt.st_size);
	close(mbrFd);
	return result;
}
