/*
 * Copyright 2019 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of opalctl.
 *
 * opalctl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * opalctl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with opalctl.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "DiskSerialNo.h"

#include <stdlib.h>
#include <unistd.h>

#include <sys/ioctl.h>
#include <string.h>

#include <linux/hdreg.h>

static size_t min(size_t a, size_t b)
{
	return a < b ? a : b;
}

bool getDiskSerialNo(int fd, char * serialNo)
{
	struct hd_driveid id;
	if (ioctl(fd, HDIO_GET_IDENTITY, &id) != -1)
	{
		const size_t s = min(sizeof(id.serial_no), DISK_SERIAL_LENGTH);
		memcpy(serialNo, id.serial_no, s);
		if (s < DISK_SERIAL_LENGTH)
			memset(serialNo + s, 0, DISK_SERIAL_LENGTH - s);

		return true;
	}

	return false;
}
