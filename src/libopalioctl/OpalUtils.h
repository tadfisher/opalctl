/*
 * Copyright 2019 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of opalctl.
 *
 * opalctl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * opalctl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with opalctl.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPAL_UTILS_H
#define OPAL_UTILS_H

#include <stdint.h>
#include <stddef.h>

/**
 * Free a null-terminated string containg a password.
 *
 * This functions overwrites the content of the string by zeros (up to the first
 * null byte) and then frees the block (by passing it to free)
 */
void freePassword(char *password);

/**
 * Free an uint8_t array containg a key.
 *
 * This functions overwrites the array by zeros (size elements) and then frees
 * the block (by passing it to free).
 */
void freeKey(uint8_t *key, size_t size);

#endif // OPAL_UTILS_H
