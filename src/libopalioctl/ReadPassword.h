/*
 * Copyright 2019 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of opalctl.
 *
 * opalctl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * opalctl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with opalctl.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef READ_PASSWORD_H
#define READ_PASSWORD_H

/**
 * Read a password from the terminal.
 *
 * The password is not echoed.
 *
 * @return The password, as a null-terminated string, or NULL in case the
 * password could not be read. The returned pointed should be freed by free (or
 * by freePassword).
 */
char * readPassword(void);

#endif // READ_PASSWORD_H
