/*
 * Copyright 2019 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of opalctl.
 *
 * opalctl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * opalctl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with opalctl.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPAL_H
#define OPAL_H

#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>

#include "OpalResult.h"

typedef enum
{
	OpalRW,
	OpalRO,
	OpalLocked
} OpalLockState;

OpalResult opalLockUnlock(int fd, const char * device, uint8_t lockingRegion, const uint8_t * key, size_t keySize, OpalLockState lockState);
OpalResult opalEnableDisableShadowMBR(int fd, const char * device, const uint8_t * key, size_t keySize, bool enabled);
OpalResult opalSetShadowMBRDone(int fd, const char * device, const uint8_t * key, size_t keySize, bool done);
OpalResult opalWriteMBR(int fd, const char * device, const uint8_t * key, size_t keySize, const char * mbrFile);

#endif // OPAL_H
