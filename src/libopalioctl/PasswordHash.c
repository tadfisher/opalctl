/*
 * Copyright 2019 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of opalctl.
 *
 * opalctl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * opalctl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with opalctl.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "PasswordHash.h"

#include "string.h"
#include "pbkdf2.h"
#include "sha1.h"

static const unsigned nIterations = 75000;

void hashPassword(uint8_t * hash, size_t hashSize, const char * password, const uint8_t * salt, size_t saltSize)
{
	cf_pbkdf2_hmac(
		(const uint8_t*)password, strlen(password),
		salt, saltSize,
		nIterations,
		hash, hashSize,
		&cf_sha1);
}
