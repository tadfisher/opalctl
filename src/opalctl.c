/*
 * Copyright 2019 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of opalctl.
 *
 * opalctl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * opalctl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with opalctl.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>

#include <getopt.h>

#include <sys/types.h>
#include <sys/stat.h>

#include "libopalioctl/ReadPassword.h"
#include "libopalioctl/PasswordHash.h"
#include "libopalioctl/DiskSerialNo.h"
#include "libopalioctl/OpalUtils.h"
#include "libopalioctl/Opal.h"

static char * programName = "opalctl";

struct CommandTag;

typedef int (*CommandFunction)(const struct CommandTag * command, int argc, char ** argv);

typedef struct CommandTag
{
	const char * name;
	const char * shortDesc;
	const char * help;
	CommandFunction f;
} Command;

static int openDevice(const char * device)
{
	/*
	 * It seems that all SED IOCTLs only care about CAP_SYS_ADMIN, so the actual
	 * fd can be opened read only which also suffices for retrieving the serial
	 * number.
	 */
	const int fd = open(device, O_RDONLY | O_NONBLOCK);
	if (fd < 0)
		fprintf(stderr, "%s: can't open device %s: %s\n", programName, device, strerror(errno));

	return fd;
}

static bool checkArgCount(int optind, int argc, int nonOptArgs)
{
	if (argc < optind + nonOptArgs)
	{
		fprintf(stderr, "%s: missing required args\n", programName);
		return false;
	}
	else if (argc > optind + nonOptArgs)
	{
		fprintf(stderr, "%s: too many arguments\n", programName);
		return false;
	}

	return true;
}

static void printCommandHelp(const Command * command)
{
	printf("%s %s %s\n", programName, command->name, command->help);
}

static void printResult(const OpalResult * result)
{
	if (result->status != OpalSuccess)
	{
		fprintf(stderr, "%s: failed: %s\n", programName, getOpalResultString(result));
	}
}

static bool getKey(int fd, const char * deviceName, bool dontHash, uint8_t ** key, size_t * keySize)
{
	printf("Enter password for %s: ", deviceName);
	char * password = readPassword();
	if (!password)
	{
		fprintf(stderr, "%s: can't read password!\n", programName);
		return false;
	}

	if (dontHash)
	{
		*key = (uint8_t*)password;
		*keySize = strlen(password);
	}
	else
	{
		char serialNo[DISK_SERIAL_LENGTH];
		if (getDiskSerialNo(fd, serialNo))
		{
			uint8_t * pwHash = (uint8_t*)malloc(SEDUTIL_PASSWORD_HASH_SIZE);
			hashPassword(pwHash, SEDUTIL_PASSWORD_HASH_SIZE, password, (const uint8_t*)(serialNo), DISK_SERIAL_LENGTH);

			*key = pwHash;
			*keySize = SEDUTIL_PASSWORD_HASH_SIZE;
		}
		else
		{
			fprintf(stderr, "%s: can't get serial number of device %s\n", programName, deviceName);
			return false;
		}

		freePassword(password);
	}

	return true;
}

static bool parseLockingRange(const char *s, uint8_t * lockingRange)
{
	int l;
	if (sscanf(s, "%i", &l) != 1 || l < 0 || l > UINT8_MAX)
	{
		fprintf(stderr, "%s: Can't parse '%s' as a number of locking range!\n", programName, s);
		return false;
	}

	*lockingRange = (uint8_t)l;
	return true;
}

static bool parseLockState(const char *s, OpalLockState * lockState)
{
	if (strcmp(s, "locked") == 0)
	{
		*lockState = OpalLocked;
		return true;
	}
	else if (strcmp(s, "ro") == 0)
	{
		*lockState = OpalRO;
		return true;
	}
	else if (strcmp(s, "rw") == 0)
	{
		*lockState = OpalRW;
		return true;
	}

	fprintf(stderr, "%s: invalid locking state value, use one of: locked, ro, rw\n", programName);
	return false;
}

static const char * stringifyLockState(OpalLockState lockState)
{
	switch (lockState)
	{
	case OpalLocked:
		return "locked";
	case OpalRO:
		return "ro";
	case OpalRW:
		return "RW";
	default:
		return "[UNKNOWN]";
	}
}

static bool parseEnableDisable(const char * s, bool * enabled)
{
	if (strcmp(s, "enabled") == 0)
	{
		*enabled = true;
		return true;
	}
	else if (strcmp(s, "disabled") == 0)
	{
		*enabled = false;
		return true;
	}

	fprintf(stderr, "%s: invalid enable/disable value, use one of: enabled, disabled\n", programName);
	return false;
}

static bool parseBool(const char * s, bool * b)
{
	if (strcmp(s, "true") == 0)
	{
		*b = true;
		return true;
	}
	else if (strcmp(s, "false") == 0)
	{
		*b = false;
		return true;
	}

	fprintf(stderr, "%s: invalid true/false value, use one of: true, false\n", programName);
	return false;
}

static int lockingState(const Command * command, int argc, char **argv)
{
	static const struct option longOptions[] =
	{
		{"help", no_argument, 0, 'h'},
		{"locking-range", required_argument, 0, 'l'},
		{"dont-hash", no_argument, 0, 'd'},
		{0, 0, 0, 0}
	};

	uint8_t lockingRange = 0;
	bool dontHash = false;

	while (true)
	{
		int optionIndex = 0;
		int c = getopt_long(argc, argv, "hl:d", longOptions, &optionIndex);
		if (c == -1)
			break;

		switch (c)
		{
		case 'h':
			printCommandHelp(command);
			return 1;
		case 'd':
			dontHash = true;
			break;
		case 'l':
			if (!parseLockingRange(optarg, &lockingRange))
				return 1;
			break;
		default:
			abort();
		}
	}

	if (!checkArgCount(optind, argc, 2))
		return 1;

	const char * device = argv[optind];
	const char * lockStateStr = argv[optind+1];

	OpalLockState lockState;
	if (!parseLockState(lockStateStr, &lockState))
		return 1;

	int fd = openDevice(device);
	if (fd < 0)
		return 1;

	uint8_t * key;
	size_t keySize;
	if (!getKey(fd, device, dontHash, &key, &keySize))
		return 1;

	printf("%s: setting locking range #%u of disk %s to %s.\n", programName,
		(unsigned)lockingRange, device, stringifyLockState(lockState));

	const OpalResult r = opalLockUnlock(fd, device, lockingRange, key, keySize, lockState);

	freeKey(key, keySize);
	close(fd);

	printResult(&r);

	return r.status != OpalSuccess;
}

int shadowMBR(const Command * command, int argc, char **argv)
{
	static const struct option longOptions[] =
	{
		{"help", no_argument, 0, 'h'},
		{"dont-hash", no_argument, 0, 'd'},
		{0, 0, 0, 0}
	};

	bool dontHash = false;

	while (true)
	{
		int optionIndex = 0;
		int c = getopt_long(argc, argv, "hd", longOptions, &optionIndex);
		if (c == -1)
			break;

		switch (c)
		{
		case 'h':
			printCommandHelp(command);
			return 1;
		case 'd':
			dontHash = true;
			break;
		default:
			abort();
		}
	}

	if (!checkArgCount(optind, argc, 2))
		return 1;

	const char * device = argv[optind];
	const char * enableDisableStr = argv[optind+1];

	bool enabled;
	if (!parseEnableDisable(enableDisableStr, &enabled))
		return 1;

	int fd = openDevice(device);
	if (fd < 0)
		return 1;

	uint8_t * key;
	size_t keySize;
	if (!getKey(fd, device, dontHash, &key, &keySize))
		return 1;

	printf("%s: %s shadow MBR for device %s\n", programName, enabled ? "enabling" : "disabling", device);

	const OpalResult r = opalEnableDisableShadowMBR(fd, device, key, keySize, enabled);

	freeKey(key, keySize);
	close(fd);

	printResult(&r);

	return r.status != OpalSuccess;
}

int MBRDone(const Command * command, int argc, char **argv)
{
	static const struct option longOptions[] =
	{
		{"help", no_argument, 0, 'h'},
		{"dont-hash", no_argument, 0, 'd'},
		{0, 0, 0, 0}
	};

	bool dontHash = false;

	while (true)
	{
		int optionIndex = 0;
		int c = getopt_long(argc, argv, "hd", longOptions, &optionIndex);
		if (c == -1)
			break;

		switch (c)
		{
		case 'h':
			printCommandHelp(command);
			return 1;
		case 'd':
			dontHash = true;
			break;
		default:
			abort();
		}
	}

	if (!checkArgCount(optind, argc, 2))
		return 1;

	const char * device = argv[optind];
	const char * mbrDoneStr = argv[optind+1];

	bool mbrDone;
	if (!parseBool(mbrDoneStr, &mbrDone))
		return 1;

	int fd = openDevice(device);
	if (fd < 0)
		return 1;

	uint8_t * key;
	size_t keySize;
	if (!getKey(fd, device, dontHash, &key, &keySize))
		return 1;

	printf("%s: setting shadow MBR done flag to %s for device %s\n", programName, mbrDone ? "true" : "false", device);

	const OpalResult r = opalSetShadowMBRDone(fd, device, key, keySize, mbrDone);

	freeKey(key, keySize);
	close(fd);

	printResult(&r);

	return r.status != OpalSuccess;
}

int writeMBR(const Command * command, int argc, char **argv)
{
	static const struct option longOptions[] =
	{
		{"help", no_argument, 0, 'h'},
		{"dont-hash", no_argument, 0, 'd'},
		{0, 0, 0, 0}
	};

	bool dontHash = false;

	while (true)
	{
		int optionIndex = 0;
		int c = getopt_long(argc, argv, "hd", longOptions, &optionIndex);
		if (c == -1)
			break;

		switch (c)
		{
		case 'h':
			printCommandHelp(command);
			return 1;
		case 'd':
			dontHash = true;
			break;
		default:
			abort();
		}
	}

	if (!checkArgCount(optind, argc, 2))
		return 1;

	const char * device = argv[optind];
	const char * mbrFileName = argv[optind+1];

	int fd = openDevice(device);
	if (fd < 0)
		return 1;

	uint8_t * key;
	size_t keySize;
	if (!getKey(fd, device, dontHash, &key, &keySize))
		return 1;

	printf("%s: writing content of file %s into the shadow MBR of device %s\n", programName, mbrFileName, device);

	const OpalResult r = opalWriteMBR(fd, device, key, keySize, mbrFileName);

	freeKey(key, keySize);
	close(fd);

	printResult(&r);

	return r.status != OpalSuccess;
}

int getSerialNo(const Command * command, int argc, char **argv)
{
	static const struct option longOptions[] =
	{
		{"help", no_argument, 0, 'h'},
		{0, 0, 0, 0}
	};

	while (true)
	{
		int optionIndex = 0;
		int c = getopt_long(argc, argv, "h", longOptions, &optionIndex);
		if (c == -1)
			break;

		switch (c)
		{
		case 'h':
			printCommandHelp(command);
			return 1;
		default:
			abort();
		}
	}

	if (!checkArgCount(optind, argc, 1))
		return 1;

	const char * device = argv[optind];

	int fd = openDevice(device);
	if (fd < 0)
		return 1;

	char serialNo[DISK_SERIAL_LENGTH];
	if (!getDiskSerialNo(fd, serialNo))
	{
		fprintf(stderr, "%s: can't get serial number of device %s!\n", programName, device);
		return 1;
	}

	printf("Serial number of %s: '", device);
	for (size_t i = 0; i < DISK_SERIAL_LENGTH; i++)
		putchar(serialNo[i]);
	puts("'");

	return 0;
}

static const Command COMMANDS[] =
{
	{
		"locking-state",
		"Lock or unlock a locking range.",
		"[-d] [-l number] device lockState\n"
		"Set locking-state of a locking range.\n"
		"-d, --dont-hash\tDo not hash password. With this option, the password is used directly"
		" as the OPAL key. Without this option, the key for each disk is created"
		" by calculating a hash of the password salted with the serial number of the"
		" disk.\n"
		"-l, --locking-range\t Specify the locking range to unlock. Defaults to 0 when"
		" not specified\n"
		"device\tthe device to lock or unlock, like /dev/sda\n"
		"lockState\tone of locked, ro or rw",
		lockingState
	},
	{
		"shadow-mbr",
		"Enable or disable shadow MBR.",
		"[-d] device enabledFlag\n"
		"Enable of disable shadow MBR.\n"
		"-d, --dont-hash\tDo not hash password. With this option, the password is used directly"
		" as the OPAL key. Without this option, the key for each disk is created"
		" by calculating a hash of the password salted with the serial number of the"
		" disk.\n"
		"-l, --locking-range\t Specify the locking range to unlock. Defaults to 0 when"
		" not specified\n"
		"device\tthe device to modify, like /dev/sda\n"
		"enabledFlag\tone of enabled or disabled",
		shadowMBR
	},
	{
		"mbr-done",
		"Set the shadow MBR done flag (hide/show shadow mbr, when shadow mbr is enabled).",
		"[-d] device mbrDoneFlag\n"
		"Set the value of the shadow MBR flag. This controls if the shadow MBR is visible (when shadow MBR is enabled).\n"
		"-d, --dont-hash\tDo not hash password. With this option, the password is used directly"
		" as the OPAL key. Without this option, the key for each disk is created"
		" by calculating a hash of the password salted with the serial number of the"
		" disk.\n"
		"device\tthe device to modify, like /dev/sda\n"
		"mbrDoneFlag one of true or false",
		MBRDone
	},
	{
		"write-mbr",
		"Write into the shadow MBR.",
		"[-d] device mbrDataFile\n"
		"Write contents of a file into the shadow MBR.\n"
		"-d, --dont-hash\tDo not hash password. With this option, the password is used directly"
		" as the OPAL key. Without this option, the key for each disk is created"
		" by calculating a hash of the password salted with the serial number of the"
		" disk.\n"
		"device\tthe device to write to, like /dev/sda\n"
		"mbrDataFile\tname of a file to write into the shadow MBR",
		writeMBR
	},
	{
		"get-disk-serial",
		"Retrieve disk serial number.",
		"device\n"
		"Retrieve device's serial number.\n"
		"device\tthe device to query, like /dev/sda",
		getSerialNo
	},
	{ NULL }
};

int help()
{
	printf("Usage: %s COMMAND args...\n\n"
		"Available commands:\n", programName);

	size_t i = 0;
	while (COMMANDS[i].name != NULL)
	{
		printf("\t%s\t%s\n", COMMANDS[i].name, COMMANDS[i].shortDesc);
		i++;
	}

	printf("\nUse %s COMMAND --help to get more info about a sub-command.\n",
		programName);

	return 0;
}

static const Command * findCommand(const char * commandName)
{
	size_t i = 0;
	while (COMMANDS[i].name != NULL && strcmp(COMMANDS[i].name, commandName) != 0)
		i++;

	return (COMMANDS[i].name == NULL) ? NULL : &COMMANDS[i];
}

int main(int argc, char ** argv)
{
	if (argc > 0)
		programName = argv[0];

	const Command * command = NULL;
	if (argc > 1)
	{
		const char * commandName = argv[1];
		if (strcmp(commandName, "help") != 0 && strcmp(commandName, "--help") != 0 && strcmp(commandName, "-h") != 0)
		{
			command = findCommand(commandName);
			if (command == NULL)
			{
				fprintf(stderr, "%s: unknown sub-command\n", programName);
				return 1;
			}
		}
	}

	if (command == NULL)
		return help();

	return (command->f)(command, argc - 1, argv + 1);
}
