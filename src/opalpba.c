/*
 * Copyright 2019 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of opalctl.
 *
 * opalctl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * opalctl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with opalctl.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdbool.h>
#include <stdlib.h>
#include <limits.h>
#include <errno.h>
#include <string.h>
#include <stdio.h>
#include <stdint.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/sysmacros.h>
#include <fcntl.h>

#include <dirent.h>
#include <unistd.h>

#include <getopt.h>

#include "libopalioctl/DiskSerialNo.h"
#include "libopalioctl/Opal.h"
#include "libopalioctl/OpalResult.h"
#include "libopalioctl/OpalUtils.h"
#include "libopalioctl/PasswordHash.h"
#include "libopalioctl/ReadPassword.h"

static char * programName = "opalpba";
static bool verbose = false;

typedef bool (*ForEachBlockDeviceCb)(const char * devFileName, const char * devName, void *userData);

static const char SYS_BLOCK[] = "/sys/block/";
static const char DEV_SUFFIX[] = "/dev";
static const char TMP_DEV_PREFIX[] = "/tmp/opalpba-dev-";

/**
 * Iterate over all block devices.
 *
 * There seem to be several ways how programs do this in linux.
 *
 * gparted seems to parse /proc/partitions, making assumptions about names:
 * https://github.com/GNOME/gparted/blob/f86d7548cdebd926bc1b2ed29a55a3df40840e78/src/Proc_Partitions_Info.cc#L56
 *
 * sedutil seems to search /dev, again depending on the names:
 * https://github.com/Drive-Trust-Alliance/sedutil/blob/358cc758948be788284d5faba46ccf4cc1813796/LinuxPBA/UnlockSEDs.cpp#L43
 *
 * Then there was (is?) libsysfs that can read that out of /sys. It seems quite
 * dead and it would also add a dependency.
 *
 * So I just iterate over /sys/block instead. This gives me the device major and
 * minor numbers, but not the device file. If there was udev, libudev could give
 * me that. But I want this to run even w/o udev. So I just create the device
 * file in /tmp. (I could search for the device in /dev, but what would be the
 * benefit, especially as this is expected to run in a dedicated initramfs?)
 */
static void forEachBlockDevice(ForEachBlockDeviceCb cb, void *userData)
{
	// check if /tmp exists
	struct stat sb;
	if (stat("/tmp", &sb) != 0 || !S_ISDIR(sb.st_mode))
	{
		fprintf(stderr, "%s: /tmp either does not exist or it's not a directory!\n", programName);
		return;
	}

	DIR * const dir = opendir(SYS_BLOCK);
	if (dir != NULL)
	{
		char sysBlockDev[sizeof(SYS_BLOCK) + 300 + sizeof(DEV_SUFFIX)];
		strcpy(sysBlockDev, SYS_BLOCK);
		char * const devName = sysBlockDev + sizeof(SYS_BLOCK) - 1;
		const size_t maxNameSize = sizeof(sysBlockDev) - sizeof(SYS_BLOCK) - sizeof(DEV_SUFFIX);

		char devFileName[sizeof(TMP_DEV_PREFIX) + 100];
		strcpy(devFileName, TMP_DEV_PREFIX);
		char * const devFileNameSuffix = devFileName + sizeof(TMP_DEV_PREFIX) - 1;
		const size_t maxDevFileNameSuffixSize = sizeof(devFileName) - sizeof(TMP_DEV_PREFIX);

		bool stop = false;
		struct dirent * entry = readdir(dir);
		while (entry != NULL && !stop)
		{
			if (strcmp(entry->d_name, "..") != 0 && strcmp(entry->d_name, "."))
			{
				const size_t nameLen = strlen(entry->d_name);
				if (nameLen < maxNameSize /* strict < to always leave space for null termination */)
				{
					memcpy(devName, entry->d_name, nameLen);
					strcpy(devName + nameLen, DEV_SUFFIX);

					FILE * const f = fopen(sysBlockDev, "r");
					if (f != NULL)
					{
						int major, minor;
						if (fscanf(f, "%d:%d", &major, &minor) == 2)
						{
							strncpy(devFileNameSuffix, entry->d_name, maxDevFileNameSuffixSize - 1);
							devFileNameSuffix[maxDevFileNameSuffixSize - 1] = 0;

							const dev_t dev = makedev(major, minor);
							if (mknod(devFileName, S_IFBLK | S_IRUSR | S_IWUSR, dev) == 0)
							{
								stop = !cb(devFileName, entry->d_name, userData);
								if (unlink(devFileName) != 0)
									fprintf(stderr, "%s: can't unlink device node %s: %s\n", programName, devFileName, strerror(errno));
							}
							else
								fprintf(stderr, "%s: can't create device node %s: %s\n", programName, devFileName, strerror(errno));
						}
						else
							fprintf(stderr, "%s: can't parse major and minor numbers from %s\n", programName, sysBlockDev);

						fclose(f);
					}
					else
						fprintf(stderr, "%s: can't open %s: %s\n", programName, sysBlockDev, strerror(errno));
				}
			}
			// else: skip

			entry = readdir(dir);
		}
		closedir(dir);
	}
	else
		fprintf(stderr, "%s: can't open %s: %s\n", programName, SYS_BLOCK, strerror(errno));
}

static void forEachBlockDeviceInList(char ** files, size_t count, ForEachBlockDeviceCb cb, void *userData)
{
	for (size_t i = 0; i < count; i++)
	{
		const char * devFileName = files[i];

		/*
		 * Test if the specified file is a block device. (Yes, this is
		 * susceptible to race condition... as are most things having to do with
		 * the fs anyway.)
		 *
		 * If a race happens here, then we could open a different file type and
		 * the OPAL IOCTLs should fail.
		 */
		struct stat sb;
		if (stat(devFileName, &sb) == 0)
		{
			if (S_ISBLK(sb.st_mode))
			{
				const char * devName = strrchr(devFileName, '/');
				if (devName == NULL || *(devName+1) == 0)
					devName = devFileName;
				else
					devName = devName+1;

				cb(devFileName, devName, userData);
			}
			else
				fprintf(stderr, "%s: '%s' is not a block device!\n", programName, devFileName);
		}
		else
			fprintf(stderr, "%s: can't stat '%s': %s\n", programName, devFileName, strerror(errno));
	}
}

static char * getPassword()
{
	/*
	 * No need to pull in ncurses dependency, this is expected to always run on
	 * a linux terminal.
	 * https://www.systutorials.com/docs/linux/man/4-console_codes/
	 */
	static const char LINUX_TERM_RESET[] = "\x1b" "c";
	printf("%s?", LINUX_TERM_RESET);

	return readPassword();
}

typedef struct
{
	uint8_t lockingRange;
	bool dontHash;
	bool userSpecifiedDevice;
	const char * password;
} UnlockBlockDeviceData;

static bool unlockBlockDevice(const char * devFileName, const char * devName, void * userData)
{
	const UnlockBlockDeviceData * d = (const UnlockBlockDeviceData*)userData;

	/*
	 * It seems that all SED IOCTLs only care about CAP_SYS_ADMIN, so the actual
	 * fd can be opened read only which also suffices for retrieving the serial
	 * number.
	 */
	const int fd = open(devFileName, O_RDONLY | O_NONBLOCK);
	if (fd != -1)
	{
		uint8_t * key = NULL;
		size_t keySize = 0;

		uint8_t pwHash[SEDUTIL_PASSWORD_HASH_SIZE];

		if (d->dontHash)
		{
			// use password as key
			key = (uint8_t*)(d->password);
			keySize = strlen(d->password);
		}
		else
		{
			/*
			 * Use sedutil-style password hash.
			 * Don't use getSedutilPasswordHash to have more control over
			 * error messages.
			 *
			 * In PBA, the fact that it's not possible to get a serial number of
			 * a device is not necessarily an error: we're enumerating all
			 * block devices and not all of them are guaranteed to be real
			 * disks. So don't print an error message in this case unless we're
			 * invoked with -v or it's a device explicitly specified by the user
			 * on the command line.
			 */
			char serialNo[DISK_SERIAL_LENGTH];
			if (getDiskSerialNo(fd, serialNo))
			{
				hashPassword(pwHash, SEDUTIL_PASSWORD_HASH_SIZE, d->password, (const uint8_t*)(serialNo), DISK_SERIAL_LENGTH);

				key = pwHash;
				keySize = sizeof(pwHash);
			}
			else if (verbose || d->userSpecifiedDevice)
				fprintf(stderr, "%s: can't get serial number\n", devName);
		}

		if (key != NULL)
		{
			// unlock
			OpalResult result = opalLockUnlock(fd, devName, d->lockingRange, key, keySize, OpalRW);

			if (result.status == OpalSuccess)
			{
				// set mbr_done
				result = opalSetShadowMBRDone(fd, devName, key, keySize, true);
				if (verbose && result.status != OpalSuccess)
					printf("%s: can't set MBR done for device %s: %s\n", programName, devName, getOpalResultString(&result));
			}
			else if (
				(verbose && result.status != OpalNotAnOpalDevice) || // don't complain when an auto-detected device is not OPAL-capable
				((verbose || result.status == OpalNotAnOpalDevice) && d->userSpecifiedDevice) || // but complain for user-specified devices
				result.status == OpalNotSupported // never suppress this error
			)
				fprintf(stderr, "%s: can't unlock: %s\n", devName, getOpalResultString(&result));
			// else: not an opal device - quietly ignore

			// invalidate pwHash
			for (size_t i = 0; i < sizeof(pwHash); i++)
				pwHash[i] = 0;
		}
		// else: was not able to get a key
	}
	else
	{
		fprintf(stderr, "can't open '%s': %s\n", devFileName, strerror(errno));
	}
	return true;
}

static const struct option longOptions[] =
{
	{"help", no_argument, 0, 'h'},
	{"dont-hash", no_argument, 0, 'd'},
	{"verbose", no_argument, 0, 'v'},
	{"locking-range", required_argument, 0, 'l'},
	{0, 0, 0, 0}
};

static const char shortOptions[] = "hdvl:";

int main(int argc, char ** argv)
{
	if (argc > 0)
		programName = argv[0];

	bool displayHelp = false;
	bool dontHash = false;
	uint8_t lockingRange = 0;

	while (true)
	{
		int optionIndex = 0;
		int c = getopt_long(argc, argv, shortOptions, longOptions, &optionIndex);
		if (c == -1)
			break;

		switch (c)
		{
		case 'h':
			displayHelp = true;
			break;
		case 'd':
			dontHash = true;
			break;
		case 'v':
			verbose = true;
			break;
		case 'l':
			{
				int l;
				if (sscanf(optarg, "%i", &l) != 1 || l < 0 || l > UINT8_MAX)
				{
					fprintf(stderr, "%s: Can't parse '%s' as a number of locking range!\n", programName, optarg);
					return 1;
				}
				lockingRange = (uint8_t)l;
			}
			break;
		default:
			abort();
		}
	}

	if (displayHelp)
	{
		printf(
			"Usage: %s [-d] [-h] [-v] [-l number] [device...]\n"
			"\n"
			"Unlock and hide shadow MBR for OPAL-capable block devices.\n"
			"\n"
			"When devices are specified on the command line, only unlock those otherwise"
			" try to unlock all block devices.\n"
			"\n"
			"-v, --verbose\tBe more verbose. By default OPAL errors and inability to get disk serial"
			" number are not logged.\n"
			"-d, --dont-hash\tDo not hash password. With this option, the password is used directly"
			" as the OPAL key. Without this option, the key for each disk is created"
			" by calculating a hash of the password salted with the serial number of the"
			" disk.\n"
			"-l, --locking-range\tSpecify the locking range to unlock. Defaults to 0 when"
			" not specified\n"
			"-h, --help\tDisplay this help\n"
			, programName);
		return 1;
	}

	char * password = getPassword();

	if (password != NULL)
	{
		UnlockBlockDeviceData d =
		{
			.lockingRange = lockingRange,
			.dontHash = dontHash,
			.userSpecifiedDevice = false,
			.password = password
		};

		if (optind < argc)
		{
			// iterate over specified block devs
			d.userSpecifiedDevice = true;
			forEachBlockDeviceInList(argv + optind, argc - optind, unlockBlockDevice, (void*)&d);
		}
		else
		{
			// iterate over all block devs
			forEachBlockDevice(unlockBlockDevice, (void*)&d);
		}

		freePassword(password);
	}
	return 0;
}
